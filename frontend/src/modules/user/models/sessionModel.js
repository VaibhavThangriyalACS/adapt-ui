// LICENCE https://github.com/adaptlearning/adapt_authoring/blob/master/LICENSE
define(['require', 'backbone', 'core/origin'], function(require, Backbone, Origin) {
  console.log("rishi");
  let params = (new URL(document.location)).search;
  let urldatas= params.substr(3);
  let detaildata = urldatas.replace(/[ =]+/g, ",").split(',')
  console.log(detaildata);
  let userEmailData = detaildata[0];
  var userEmailId = userEmailData.substr(0,userEmailData.length-4)
 var SessionModel = Backbone.Model.extend({
   url: "api/authcheck",
   defaults: {
     id: '',
     tenantId: '',
     email: '',
     isAuthenticated: false,
     permissions: [],
     otherLoginLinks: []
   },
   initialize: function() {
     setTimeout(() => {
       this.postRender();
     }, 1000);
     },
   postRender: function() {
     this.login();
     if( window.localStorage ){
       if(!localStorage.getItem('firstReLoad')){
        localStorage['firstReLoad'] = true;
        window.location.reload();
       } else {
        localStorage.removeItem('firstReLoad');
       }
     }
   },
   login: function (username, password, shouldPersist) {
     var postData = {
       email: userEmailId,
       password: 'test123',
       shouldPersist: false
     };
     $.post('api/login', postData, _.bind(function (jqXHR, textStatus, errorThrown) {
       this.set({
         id: jqXHR.id,
         tenantId: jqXHR.tenantId,
         email: jqXHR.email,
         isAuthenticated: true,
         permissions: jqXHR.permissions
       });
       Origin.trigger('login:changed');
       Origin.trigger('schemas:loadData', Origin.router.navigateToHome);
     }, this)).fail(function(jqXHR, textStatus, errorThrown) {
       Origin.trigger('login:failed', (jqXHR.responseJSON && jqXHR.responseJSON.errorCode) || 1);
     });
   },
   logout: function () {
     $.post('api/logout', _.bind(function() {
       // revert to the defaults
       this.set(this.defaults);
       Origin.trigger('login:changed');
       // Origin.router.navigateToLogin();
       window.location.href = 'http://localhost:4200/';
     }, this));
   },
 });
 return SessionModel;
});