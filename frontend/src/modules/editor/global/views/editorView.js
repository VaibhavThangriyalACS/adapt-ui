// LICENCE https://github.com/adaptlearning/adapt_authoring/blob/master/LICENSE

const { object, keys } = require('underscore');
var moment = require('moment');
/*bx
 * TODO I think this exists to add extra functionality to the menu/page structure pages
 */
define(function (require) {
  var Backbone = require('backbone');
  var Origin = require('core/origin');
  var helpers = require('core/helpers');

  var EditorOriginView = require('./editorOriginView');
  var EditorMenuView = require('../../contentObject/views/editorMenuView');
  var EditorPageView = require('../../contentObject/views/editorPageView');

  var ContentObjectModel = require('core/models/contentObjectModel');
  var ArticleModel = require('core/models/articleModel');
  var BlockModel = require('core/models/blockModel');
  var ComponentModel = require('core/models/componentModel');
  var UserProfileModel = require('../../../user/models/userProfileModel');
  var $loading;
  const toBase64 = file => new Promise((resolve, reject) => {
    var formData = new FormData();
    formData.append('download.zip', file);
    const reader = new FileReader();
    let blobobj = new Blob([formData], { type: 'application/zip' })
    reader.readAsDataURL(blobobj);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  deleteRowTable = function name(params) {
    let dataobj = {
      "historyId": params.historyId,
      "modelName": params.modelName
    }
    $.ajax({
      type: 'POST',
      url: "api/user/revert",
      data: JSON.stringify(dataobj),
      success: function (response) {
        if (response.status === 200) {
          return true;
        }
      },
      error: function (e) {
        console.log(e);
        $loading.addClass('display-out');
        Origin.editor.isDownloadPending = false;
        Origin.Notify.alert({
          type: 'error',
          title: "Error while revertion",
        });
      },
      contentType: "application/json; charset=utf-8", // this
      dataType: "json", // and this
    });

  }

  getnewDate = function formatDate(date) {

    return date.getDate() + '/' +
      (date.getMonth() + 1) + '/' +
      date.getFullYear() + ' ' +
      date.getHours() + ':' +
      date.getMinutes();
  }

  var EditorView = EditorOriginView.extend({
    className: "editor-view",
    tagName: "div",

    settings: {
      autoRender: false
    },
    exporting: false,

    events: {
      "click a.page-add-link": "addNewPage",
      "click a.load-page": "loadPage",
      "mouseover div.editable": "onEditableHoverOver",
      "mouseout div.editable": "onEditableHoverOut",
      'click button.uploadFormSubmit': 'uploadFormSubmit',
      'click button.modelBoxCloseRevision': 'closePopupRevisionBoxes',
      'click button.modelBoxClose': 'closePopupBoxes',
      'click button.modelBoxCloseDetail': 'closePopupRevisionDetailBoxes',
      'click button.revertSubmit': 'revertSubmit',

    },


    CreateCourseLMS: async function (error) {
      if (error) return;
      $loading = $('.loading');
      $loading.removeClass('display-none fade-out display-out')
      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');
      var self = this;
      var profile = new UserProfileModel();
      let newparams = (new URL(window.location)).search;
      let newurldatas = newparams.substr(3);
      let Newdetaildata = newurldatas.replace(/[ =]+/g, ",").split(',');
      let entityNameData = Newdetaildata[1];
      let userIDLogLMS = Newdetaildata[2];
      var entityName = entityNameData.substr(0, entityNameData.length - 7);
      await profile.fetch({
        success: function (response) {
        }
      });
      var userDataID = JSON.stringify(profile);
      var userID = JSON.parse(userDataID)._id;

      $.ajax({
        url: 'api/content/course/' + courseId,
        success: function (data, textStatus, jqXHR) {
          var searchCourse = {
            "courseCreatorID": courseId
          }
          searchCourse.entityParms = entityName;
          searchCourse.userIDLogLMS = userIDLogLMS;
          $.ajax({
            url: 'api/user/findLmsCourse',
            type: 'POST',
            data: searchCourse,
            success: function (response) {
              if (response.status === 400) {
                var courseTitleLMS = data.title;
                var courseDescLMS = data.description;
                var filenametoupload = courseTitleLMS.replace(/\s/g, "");
                $.ajax({
                  url: 'export/' + tenantId + '/' + courseId,
                  success: function (data, textStatus, jqXHR) {
                    var form = document.createElement('form');
                    self.$el.append(form);
                    form.setAttribute('action', 'export/' + tenantId + '/' + courseId + '/download.zip');
                    if (Origin.editor.isDownloadPending) {
                      return;
                    }
                    var url = 'api/output/' + Origin.constants.outputPlugin + '/publish/' + courseId;
                    $.get(url, function (data, textStatus, jqXHR) {
                      if (!data.success) {
                        Origin.Notify.alert({
                          type: 'error',
                          text: Origin.l10n.t('app.errorgeneric') +
                            Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
                        });
                        Origin.editor.isDownloadPending = false;
                        return;
                      }
                      const pollUrl = data.payload && data.payload.pollUrl;
                      if (pollUrl) {
                        return;
                      }
                      Origin.editor.isDownloadPending = false;
                      toBase64(data.payload.filename).then(result => {
                        var documentData = {
                          pathValue: data.payload.filename,
                          fileNameZip: filenametoupload,
                          courseName: courseTitleLMS,
                          courseDesc: courseDescLMS,
                          courseCreatorID: courseId,
                          creatorUserIDCourse: userID,
                          userIDLogLMS: userIDLogLMS,
                          entityParms: entityName
                        }
                        $.ajax({
                          url: 'api/user/zipContentblob',
                          type: 'POST',
                          data: documentData,
                          success: function (response) {
                            $loading.addClass('display-out');
                            Origin.Notify.alert({
                              type: 'success',
                              title: Origin.l10n.t('app.successdefaulttitle'),
                              text: Origin.l10n.t('Course Created successfully.')
                            });
                          },
                          error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $loading.addClass('display-out');
                            Origin.Notify.alert({
                              type: 'error',
                              title: Origin.l10n.t('app.exporterrortitle'),
                              text: Origin.l10n.t('app.errorgeneric') +
                                Origin.l10n.t('app.debuginfo', { message: errorThrown })
                            });
                          }
                        });
                      });
                    }.bind(this)).fail(function (jqXHR, textStatus, errorThrown) {
                      $loading.addClass('display-out');
                      Origin.editor.isDownloadPending = false;
                      Origin.Notify.alert({ type: 'error', text: Origin.l10n.t('app.errorgeneric') });
                    }.bind(this));
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                    $loading.addClass('display-out');
                    Origin.Notify.alert({
                      type: 'error',
                      title: Origin.l10n.t('app.exporterrortitle'),
                      text: Origin.l10n.t('app.errorgeneric') +
                        Origin.l10n.t('app.debuginfo', { message: "Server Down" })
                    });
                  }
                });
              } else {
                self.exporting = true;
                var courseTitleLMS = data.title;
                var courseDescLMS = data.description;
                var filenametoupload = courseTitleLMS.replace(/\s/g, "");
                $.ajax({
                  url: 'export/' + tenantId + '/' + courseId,
                  success: function (data, textStatus, jqXHR) {
                    var form = document.createElement('form');
                    self.$el.append(form);
                    form.setAttribute('action', 'export/' + tenantId + '/' + courseId + '/download.zip');
                    // form.submit();
                    if (Origin.editor.isDownloadPending) {
                      $loading.addClass('display-out');
                      return;
                    }
                    var url = 'api/output/' + Origin.constants.outputPlugin + '/publish/' + courseId;
                    $.get(url, function (data, textStatus, jqXHR) {
                      if (!data.success) {
                        $loading.addClass('display-out');
                        Origin.Notify.alert({
                          type: 'error',
                          text: Origin.l10n.t('app.errorgeneric') +
                            Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
                        });
                        Origin.editor.isDownloadPending = false;
                        return;
                      }
                      const pollUrl = data.payload && data.payload.pollUrl;
                      if (pollUrl) {
                        return;
                      }
                      toBase64(data.payload.filename).then(result => {
                        var documentData = {
                          pathValue: data.payload.filename,
                          fileNameZip: filenametoupload,
                          courseName: courseTitleLMS,
                          courseDesc: courseDescLMS,
                          courseCreatorID: courseId,
                          creatorUserIDCourse: userID,
                          userIDLogLMS: userIDLogLMS,
                          entityParms: entityName
                        }
                        $.ajax({
                          url: 'api/user/zipContentblobForUpdate',
                          type: 'POST',
                          data: documentData,
                          success: function (response) {
                            Origin.editor.isDownloadPending = false;
                            $loading.addClass('display-out');
                            Origin.Notify.alert({
                              type: 'success',
                              title: Origin.l10n.t('app.successdefaulttitle'),
                              text: Origin.l10n.t('Course Updated successfully.')
                            });
                          },
                          error: function (XMLHttpRequest, textStatus, errorThrown) {
                            Origin.editor.isDownloadPending = false;
                            $loading.addClass('display-out');
                            Origin.Notify.alert({
                              type: 'error',
                              title: Origin.l10n.t('app.exporterrortitle'),
                              text: Origin.l10n.t('app.errorgeneric') +
                                Origin.l10n.t('app.debuginfo', { message: errorThrown })
                            });
                          }

                        });
                      });
                    }.bind(this)).fail(function (jqXHR, textStatus, errorThrown) {
                      $loading.addClass('display-out');
                      Origin.editor.isDownloadPending = false;
                      Origin.Notify.alert({ type: 'error', text: Origin.l10n.t('app.errorgeneric') });
                    }.bind(this));
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                    $loading.addClass('display-out');
                    Origin.editor.isDownloadPending = false;
                    Origin.Notify.alert({
                      type: 'error',
                      title: Origin.l10n.t('app.exporterrortitle'),
                      text: Origin.l10n.t('app.errorgeneric') +
                        Origin.l10n.t('app.debuginfo', { message: "Server Down" })
                    });
                  }
                });
              }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
              $loading.addClass('display-out');
              Origin.editor.isDownloadPending = false;
              Origin.Notify.alert({
                type: 'error',
                title: Origin.l10n.t('app.exporterrortitle'),
                text: Origin.l10n.t('app.errorgeneric') +
                  Origin.l10n.t('app.debuginfo', { message: errorThrown })
              });
            }
          });
        }, error: function (jqXHR, textStatus, errorThrown) {
          $loading.addClass('display-out');
          Origin.editor.isDownloadPending = false;
          Origin.Notify.alert({
            type: 'error',
            title: Origin.l10n.t('app.exporterrortitle'),
            text: Origin.l10n.t('app.errorgeneric') +
              Origin.l10n.t('app.debuginfo', { message: "Server Down" })
          });
        }
      });
    },


    preRender: function (options) {
      this.currentView = options.currentView;
      Origin.editor.isPreviewPending = false;
      this.currentCourseId = Origin.editor.data.course.get('_id');
      this.currentCourse = Origin.editor.data.course;
      this.currentPageId = options.currentPageId;
      this.listenTo(Origin, {
        'editorView:refreshView': this.setupEditor,
        'editorView:copy': this.addToClipboard,
        'editorView:copyID': this.copyIdToClipboard,
        'editorView:paste': this.pasteFromClipboard,
        'editorCommon:download2004': this.downloadProject2004,
        'editorCommon:download': function () {
          this.validateProject(function (error) {
            this.downloadProject();
          });
        },
        'editorCommon:CourseLMS': function () {
          this.validateProject(function (error) {
            this.CreateCourseLMS();
          });
        },

        'editorCommon:languageCourseLMS': function () {
          this.validateProject(function (error) {
            this.languageCourseLMS();
          });
        },

        'editorCommon:openPopupBoxes': function () {
          this.validateProject(function (error) {
            this.openPopupBoxes();
          });
        },


        'editorCommon:uploadFormSubmit': function () {
          this.uploadFormSubmit();
        },

        'editorCommon:revertSubmit': function () {
          this.revertSubmit();
        },



        'editorCommon:closePopupBoxes': function () {
          this.validateProject(function (error) {
            this.closePopupBoxes();
          });
        },


        'editorCommon:openPopupRevisionBoxes': function () {
          this.validateProject(function (error) {
            this.openPopupRevisionBoxes();
          });
        },

        'editorCommon:closePopupRevisionBoxes': function () {
          this.validateProject(function (error) {
            this.closePopupRevisionBoxes();
          });
        },


        'editorCommon:closePopupRevisionDetailBoxes': function () {
          this.closePopupRevisionDetailBoxes();
        },

        'editorCommon:preview': function (isForceRebuild) {
          var previewWindow = window.open('loading', 'preview');
          this.validateProject(function (error) {
            if (error) {
              return previewWindow.close();
            }
            this.previewProject(previewWindow, isForceRebuild);
          });
        },
        'editorCommon:export': function () {
          this.validateProject(function (error) {
            this.exportProject(error);
          });
        }
      });
      this.render();
      this.setupEditor();
    },


    postRender: function () {
      let newparamsPre = (new URL(window.location)).search;
      let newurldatasPre = newparamsPre.substr(3);
      let NewdetaildataPre = newurldatasPre.replace(/[ =]+/g, ",").split(',');
      let entityNameDataPre = NewdetaildataPre[1];
      var entityNamePre = entityNameDataPre.substr(0, entityNameDataPre.length - 7);
      let userIDLogLMS = NewdetaildataPre[2];
      $loading = $('.loading');
      $loading.removeClass('display-none fade-out display-out');
      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');
      var courseVarData = {
        courseCreatorID: courseId
      }
      courseVarData.entityParms = entityNamePre;
      courseVarData.userIDLogLMS = userIDLogLMS;
      $.ajax({
        url: 'api/user/findLmsCourse',
        type: 'POST',
        data: courseVarData,
        success: function (response) {
          if (response.status === 400) {
            $loading.addClass('display-out');
            $('.lang.create').addClass('hide');
          } else {
            $loading.addClass('display-out');
            $('.gencourse.update').addClass('hide');
          }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          $loading.addClass('display-out');
        }
      });
    },

    setupEditor: function () {
      this.renderCurrentEditorView();
    },

    validateProject: function (next) {
      next.call(this);
      // helpers.validateCourseContent(this.currentCourse, _.bind(function (error) {
      //   if (error) {
      //     Origin.Notify.alert({ type: 'error', text: "There's something wrong with your course:<br/><br/>" + error });
      //   }
      //   next.call(this, error);
      // }, this));
    },

    previewProject: function (previewWindow, forceRebuild) {
      if (Origin.editor.isPreviewPending) {
        return;
      }
      Origin.editor.isPreviewPending = true;
      $('.navigation-loading-indicator').removeClass('display-none');
      $('.editor-common-sidebar-preview-inner').addClass('display-none');
      $('.editor-common-sidebar-previewing').removeClass('display-none');

      var url = 'api/output/' + Origin.constants.outputPlugin + '/preview/' + this.currentCourseId + '?force=' + (forceRebuild === true);
      $.get(url, function (data, textStatus, jqXHR) {
        if (!data.success) {
          this.resetPreviewProgress();
          Origin.Notify.alert({
            type: 'error',
            text: Origin.l10n.t('app.errorgeneratingpreview') +
              Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
          });
          previewWindow.close();
          return;
        }
        const pollUrl = data.payload && data.payload.pollUrl;
        if (pollUrl) {
          // Ping the remote URL to check if the job has been completed
          this.updatePreviewProgress(pollUrl, previewWindow);
          return;
        }
        this.updateCoursePreview(previewWindow);
        this.resetPreviewProgress();
      }.bind(this)).fail(function (jqXHR, textStatus, errorThrown) {
        this.resetPreviewProgress();
        Origin.Notify.alert({ type: 'error', text: Origin.l10n.t('app.errorgeneric') });
        previewWindow.close();
      }.bind(this));
    },

    exportProject: function (error) {
      // TODO - very similar to export in project/views/projectView.js, remove duplication
      // aleady processing, don't try again
      if (error || this.exporting) return;

      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');

      var $btn = $('button.editor-common-sidebar-export');

      this.showExportAnimation(true, $btn);
      this.exporting = true;

      var self = this;
      $.ajax({
        url: 'export/' + tenantId + '/' + courseId,
        success: function (data, textStatus, jqXHR) {
          self.showExportAnimation(false, $btn);
          self.exporting = false;

          // get the zip
          var form = document.createElement('form');
          self.$el.append(form);
          form.setAttribute('action', 'export/' + tenantId + '/' + courseId + '/download.zip');
          // form.submit();
          if (Origin.editor.isDownloadPending) {
            return;
          }
          $('.editor-common-sidebar-download-inner').addClass('display-none');
          $('.editor-common-sidebar-downloading').removeClass('display-none');

          var url = 'api/output/' + Origin.constants.outputPlugin + '/publish/' + courseId;
          $.get(url, function (data, textStatus, jqXHR) {
            if (!data.success) {
              Origin.Notify.alert({
                type: 'error',
                text: Origin.l10n.t('app.errorgeneric') +
                  Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
              });
              // this.resetDownloadProgress();
              $('.editor-common-sidebar-download-inner').removeClass('display-none');
              $('.editor-common-sidebar-downloading').addClass('display-none');
              return;
            }
            const pollUrl = data.payload && data.payload.pollUrl;
            if (pollUrl) {
              // Ping the remote URL to check if the job has been completed
              // this.updateDownloadProgress(pollUrl);
              return;
            }
            // this.resetDownloadProgress();
            $('.editor-common-sidebar-download-inner').removeClass('display-none');
            $('.editor-common-sidebar-downloading').addClass('display-none');
            var $downloadForm = $('#downloadForm');
            $downloadForm.attr('action', 'download/' + Origin.sessionModel.get('tenantId') + '/' + Origin.editor.data.course.get('_id') + '/' + data.payload.zipName + '/download.zip');
            $downloadForm.submit();

          }.bind(this)).fail(function (jqXHR, textStatus, errorThrown) {
            this.resetDownloadProgress();
            Origin.Notify.alert({ type: 'error', text: Origin.l10n.t('app.errorgeneric') });
          }.bind(this));
        },
        error: function (jqXHR, textStatus, errorThrown) {
          self.showExportAnimation(false, $btn);
          self.exporting = false;

          Origin.Notify.alert({
            type: 'error',
            title: Origin.l10n.t('app.exporterrortitle'),
            text: Origin.l10n.t('app.errorgeneric') +
              Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
          });
        }
      });
    },

    languageCourseLMS: function (error) {
      $('.editor-common-sidebar-courselanguage').addClass('display-none');
      $('.editor-common-sidebar-courselanguageprocessing').removeClass('display-none');
      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');
      var $btn = $('button.editor-common-sidebar-export');
      this.showExportAnimation(true, $btn);
      this.exporting = true;
      var self = this;
      $.ajax({
        url: 'export/' + tenantId + '/' + courseId,
        success: function (data, textStatus, jqXHR) {
          self.showExportAnimation(false, $btn);
          self.exporting = false;

          // get the zip
          var form = document.createElement('form');
          self.$el.append(form);
          form.setAttribute('action', 'export/' + tenantId + '/' + courseId + '/download.zip');
          // form.submit();
          if (Origin.editor.isDownloadPending) {
            return;
          }
          $('.editor-common-sidebar-download-inner').addClass('display-none');
          $('.editor-common-sidebar-downloading').removeClass('display-none');

          var url = 'api/output/' + Origin.constants.outputPlugin + '/publish/' + courseId;
          $.get(url, function (data, textStatus, jqXHR) {
            if (!data.success) {
              Origin.Notify.alert({
                type: 'error',
                text: Origin.l10n.t('app.errorgeneric') +
                  Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
              });
              // this.resetDownloadProgress();
              $('.editor-common-sidebar-courselanguageprocessing').addClass('display-none');
              $('.editor-common-sidebar-courselanguage').removeClass('display-none');
              $('.editor-common-sidebar-download-inner').removeClass('display-none');
              $('.editor-common-sidebar-downloading').addClass('display-none');
              return;
            }
            const pollUrl = data.payload && data.payload.pollUrl;
            if (pollUrl) {
              $('.editor-common-sidebar-courselanguageprocessing').addClass('display-none');
              $('.editor-common-sidebar-courselanguage').removeClass('display-none');
              // Ping the remote URL to check if the job has been completed
              // this.updateDownloadProgress(pollUrl);
              return;
            }
            $('.editor-common-sidebar-courselanguageprocessing').addClass('display-none');
            $('.editor-common-sidebar-courselanguage').removeClass('display-none');
            // this.resetDownloadProgress();
            $('.editor-common-sidebar-download-inner').removeClass('display-none');
            $('.editor-common-sidebar-downloading').addClass('display-none');

            Origin.Notify.alert({
              type: 'success',
              title: "success",
              text: "File will be sent to your e-mail"
            });
            var searchCoursePath = {
              courseId, tenantId
            }
            $.ajax({
              url: 'api/user/language',
              type: 'POST',
              data: searchCoursePath,
              timeout: 420000,
              success: function (response) {
                $('.editor-common-sidebar-courselanguageprocessing').addClass('display-none');
                $('.editor-common-sidebar-courselanguage').removeClass('display-none');
                window.location = '/api/downloadzip';
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('.editor-common-sidebar-courselanguageprocessing').addClass('display-none');
                $('.editor-common-sidebar-courselanguage').removeClass('display-none');
                $loading.addClass('display-out');
              }
            });

          }.bind(this)).fail(function (jqXHR, textStatus, errorThrown) {
            this.resetDownloadProgress();
            $('.editor-common-sidebar-courselanguageprocessing').addClass('display-none');
            $('.editor-common-sidebar-courselanguage').removeClass('display-none');
            Origin.Notify.alert({ type: 'error', text: Origin.l10n.t('app.errorgeneric') });
          }.bind(this));
        },
        error: function (jqXHR, textStatus, errorThrown) {
          self.showExportAnimation(false, $btn);
          self.exporting = false;
          $('.editor-common-sidebar-courselanguageprocessing').addClass('display-none');
          $('.editor-common-sidebar-courselanguage').removeClass('display-none');

          Origin.Notify.alert({
            type: 'error',
            title: Origin.l10n.t('app.exporterrortitle'),
            text: Origin.l10n.t('app.errorgeneric') +
              Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
          });
        }
      });
    },

    openPopupBoxes: function (error) {
      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');
      $('.modelBoxPopup').addClass('showModel');
    },

    closePopupBoxes: function (error) {
      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');
      $('.modelBoxPopup').removeClass('showModel');
    },

    uploadFormSubmit: function (error) {
      // error && error.preventDefault();
      $('.uploadFormSubmit').addClass('display-none');
      $('.uploadFormSubmitandProcess').removeClass('display-none');
      let newparams = (new URL(window.location)).search;
      let newurldatas = newparams.substr(3);
      let Newdetaildata = newurldatas.replace(/[ =]+/g, ",").split(',');
      let entityNameData = Newdetaildata[1];
      let userIDLogLMS = Newdetaildata[2];
      var entityName = entityNameData.substr(0, entityNameData.length - 7);


      var language_code = $.trim(document.getElementById('language_code').value);
      var courseID = Origin.editor.data.course.get('_id');
      var courseName = Origin.editor.data.course.get('displayTitle')
      if (language_code == '' || document.getElementById('course_file').files.length === 0 || document.getElementById('content_object_file').files.length === 0
        || document.getElementById('component_file').files.length === 0 || document.getElementById('article_file').files.length === 0 ||
        document.getElementById('block_file').files.length === 0) {
        var errorMessage = 'Please Select All The Files'
        $('#uploadFormErrorMessage').text(errorMessage);
        $('#uploadFormError').removeClass('display-none');
        this.resetUploadAndProgress();
        return false;
      } else {
        $('#uploadFormError').addClass('display-none');
        var formData = new FormData();
        formData.append('courseName', courseName);
        formData.append('userIDLogLMS', userIDLogLMS);
        formData.append('entityName', entityName);
        formData.append('language_code', language_code);
        formData.append('courseID', courseID);
        formData.append('course_file', document.getElementById('course_file').files[0]);
        formData.append('content_object_file', document.getElementById('content_object_file').files[0]);
        formData.append('component_file', document.getElementById('component_file').files[0]);
        formData.append('article_file', document.getElementById('article_file').files[0]);
        formData.append('block_file', document.getElementById('block_file').files[0]);
        $.ajax({
          url: 'api/user/importLang',
          type: 'POST',
          cache: false,
          contentType: false,
          processData: false,
          data: formData,
          success: function (response) {
            if (response.status === 200) {
              Origin.Notify.alert({
                type: 'success',
                title: "success",
                text: "Language Imported successfully"
              });
              this.resetUploadAndProgress();
            }
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            $loading.addClass('display-out');
            this.resetUploadAndProgress();
          }
        });
      }
    },

    openPopupRevisionBoxes: function (error) {

      $('.modelBoxPopupRevision').addClass('showModel');
      this.getAlltableData();
      var coll = document.getElementsByClassName("collapsible");
      var i;

      for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function () {
          this.classList.toggle("active");
          var content = this.nextElementSibling;
          if (content.style.display == 'block') {
            content.style.display = 'none';
          } else {
            content.style.display = 'block';
          }
        });
      }

    },

    getAlltableData: function (params) {
      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');
      let getObj = { "courseId": courseId }
      var self = this;
      $.ajax({
        url: 'api/gethistory',
        type: 'POST',
        data: getObj,
        timeout: 420000,
        success: function (response) {
          console.log(response);
          if (response.status == 200) {
            var newArray = [...response.data[0].courses_history, ...response.data[0].blocks_history, ...response.data[0].components_history, ...response.data[0].contentobjects_history, ...response.data[0].courses_history]
            console.log('Merged Array:', newArray);
            self.createList(newArray);
          }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          $loading.addClass('display-out');
        }
      });
    },

    createList: function (response) {
      var self = this;
      self.initDataTableWithStyle('rivisionTable');
      response.sort((a, b) => Date.parse(b.t) - Date.parse(a.t));
      console.log('Reveresed Array:', response);
      for (let index = 0; index < response.length; index++) {
        let element = response[index];
        var tableRef = document.getElementById('rivisionTable').getElementsByTagName('tbody')[0];
        var backgroundColor;
        switch (element.operation) {
          case 'i':
            element.operation = 'insert';
            backgroundColor = '#bdebb9';
            break;
          case 'u':
            element.operation = 'update'
            backgroundColor = '#ffffff';
            break;
          case 'r':
            element.operation = 'remove'
            backgroundColor = '#ebb9c1';
            break;
          default:
            break;
        }
        if (!element.current) {
          return console.log('Using Older Version of Mongoose History');
        }
        let sno = document.createTextNode(index + 1);
        let reverted = element.reverted ? document.createTextNode('Reverted') : document.createTextNode('Not Reverted');
        if (element.current._type == 'page') { element.current._type = 'contentobject'; }
        let componentName = document.createTextNode(element.current.title);
        let type = document.createTextNode((element.current._type).toUpperCase());
        let operationType = document.createTextNode((element.operation).toUpperCase());
        let updatedOn = document.createTextNode(moment(element.t).format('D/M/YY HH:mm'));
        let updatedBy = (element.updatedBy && element.updatedBy.length) ? document.createTextNode(element.updatedBy[0].email) : document.createTextNode('Not Available');
        let newRow = tableRef.insertRow();
        newRow.style.cssText = `background-color: ${backgroundColor}`
        // newCell1.appendChild(sno);
        newRow.insertCell(0).appendChild(componentName);
        newRow.insertCell(1).appendChild(type);
        newRow.insertCell(2).appendChild(operationType);
        newRow.insertCell(3).appendChild(updatedOn);
        newRow.insertCell(4).appendChild(updatedBy);
        newRow.insertCell(5).appendChild(reverted);
        var btn = document.createElement('input');
        btn.type = "button";
        btn.value = 'Info'
        btn.className = "btn btn-sm btn btn-outline-secondary";
        btn.onclick = function () {
          self.showModalDetails(element)
        };
        newRow.insertCell(6).appendChild(btn);
      }
    },

    showModalDetails: function (element) {
      var self = this;
      $('.modelBoxPopupDetail').addClass('showModel');
      let revertArray = [];
      keys(element.updated).forEach(property => {
        obj = {};
        if (property !== '_id') {
          obj.property = property;
          obj.current = element.current[property];
          obj.old = element.old ? element.old[property] : 'Not Available';
          obj.updatedOn = element.t;
          revertArray.push(obj);
        }
      });
      revertArray.sort((a, b) => Date.parse(a.updatedOn) - Date.parse(b.updatedOn));
      console.log('Reveresed Internal Array:', revertArray);
      var tableRef = document.getElementById('detailedTable').getElementsByTagName('tbody')[0];
      $('#detailedTable').DataTable().clear().destroy();
      self.initDataTableWithStyle('detailedTable');
      revertArray.forEach((element, index) => {
        let newRow = tableRef.insertRow();
        newRow.insertCell(0).innerHTML = index + 1;
        newRow.insertCell(1).innerHTML = element.property;
        newRow.insertCell(2).innerHTML = element.current;
        newRow.insertCell(3).innerHTML = element.old;
        newRow.insertCell(4).innerHTML = moment(element.updatedOn).format('D/M/YY HH:mm');
        // newRow.insertCell(6).innerHTML = updatedOn.setAttribute('data-sort', 'YYYYMMDD');
      });
      var btn = document.getElementById('revertButton');
      btn.type = "button";
      if (!element.reverted) {
        btn.value = 'Revert'
        btn.className = "btn btn-sm btn btn-outline-secondary";
        $('#revertingNotPossibleButton').addClass('display-none')
        btn.onclick = function () {
          self.submitRevert(element._id, element.current._type);
        }
      } else {
        btn.value = 'Revert Not Possible'
        btn.className = "btn btn-sm btn btn-secondary display-none";
        btn.disabled = true;
        $('#revertingNotPossibleButton').removeClass('display-none')
      }
    },

    initDataTableWithStyle: function (tableName) {
      $(`#${tableName} > tbody`).html("");
      $(document).ready(function () {
        $(`#${tableName}`).DataTable({
          // "aaSorting": [[3, 'asc']],
          "bDestroy": true,
        });
        $(`#${tableName}_wrapper`).css("width", "-webkit-fill-available");
        $(`select[name ="${tableName}_length"]`).addClass('form-control-sm');
        $(`#${tableName}_filter label`).css('display', 'flex');
        $(`#${tableName}_filter label input`).addClass('form-control-sm');
      });
    },

    submitRevert: function (id, model) {
      self.showLoader('revertButton', 'reevertingButton', 'show');
      let historyID = id;
      let modelName = model;
      if (modelName == 'page') {
        modelName = 'contentobject';
      }
      $.ajax({
        url: 'api/reverHistory',
        type: 'POST',
        data: { historyID, modelName },
        timeout: 420000,
        success: function (response) {
          if (response.status == 200) {
            self.showLoader('revertButton', 'reevertingButton', 'hide');
            Origin.editor.isDownloadPending = false;
            Origin.Notify.alert({
              type: 'success',
              title: "Successfully reverted data",
            });
          }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          self.showLoader('revertButton', 'reevertingButton', 'hide');
          $loading.addClass('display-out');
          Origin.editor.isDownloadPending = false;
          Origin.Notify.alert({
            type: 'error',
            title: "Error while revertion",
          });
        }
      });
    },

    revertSubmit: function (params) {
      let historyID = document.getElementById("historyIdIndex").value;
      let modelName = document.getElementById("content_TypeIdex").value;
      let getObj = { "historyId": historyID, "modelName": modelName }
      $.ajax({
        url: 'api/reverHistory',
        type: 'POST',
        data: getObj,
        timeout: 420000,
        success: function (response) {
          if (response.status == 200) {
            $('.modelBoxPopupRevision').removeClass('showModel');
            $('.modelBoxPopupDetail').removeClass('showModel');
            Origin.editor.isDownloadPending = false;
            Origin.Notify.alert({
              type: 'success',
              title: "Successfully reverted data",
            });
          }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          $loading.addClass('display-out');
          Origin.editor.isDownloadPending = false;
          Origin.Notify.alert({
            type: 'error',
            title: "Error while revertion",
          });
        }
      });
    },

    closePopupRevisionBoxes: function (error) {
      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');
      $('.modelBoxPopupRevision').removeClass('showModel');
    },

    closePopupRevisionDetailBoxes: function (error) {
      $('.modelBoxPopupDetail').removeClass('showModel');
    },

    uploadLangFailed: function (errorCode) {
      var errorMessage = '';
      console.log(errorCode, errorMessage)
    },

    showExportAnimation: function (show, $btn) {
      if (show !== false) {
        $('.editor-common-sidebar-export-inner', $btn).addClass('display-none');
        $('.editor-common-sidebar-exporting', $btn).removeClass('display-none');
      } else {
        $('.editor-common-sidebar-export-inner', $btn).removeClass('display-none');
        $('.editor-common-sidebar-exporting', $btn).addClass('display-none');
      }
    },

    downloadProject: function () {
      if (Origin.editor.isDownloadPending) {
        return;
      }
      $('.editor-common-sidebar-download-inner').addClass('display-none');
      $('.editor-common-sidebar-downloading').removeClass('display-none');
      var url = 'api/output/' + Origin.constants.outputPlugin + '/publish/' + this.currentCourseId;
      $.get(url, function (data, textStatus, jqXHR) {
        if (!data.success) {
          Origin.Notify.alert({
            type: 'error',
            text: Origin.l10n.t('app.errorgeneric') +
              Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
          });
          this.resetDownloadProgress();
          return;
        }
        const pollUrl = data.payload && data.payload.pollUrl;
        if (pollUrl) {
          // Ping the remote URL to check if the job has been completed
          this.updateDownloadProgress(pollUrl);
          return;
        }
        this.resetDownloadProgress();

        var $downloadForm = $('#downloadForm');
        $downloadForm.attr('action', 'download/' + Origin.sessionModel.get('tenantId') + '/' + Origin.editor.data.course.get('_id') + '/' + data.payload.zipName + '/download.zip');
        $downloadForm.submit();

      }.bind(this)).fail(function (jqXHR, textStatus, errorThrown) {
        this.resetDownloadProgress();
        Origin.Notify.alert({ type: 'error', text: Origin.l10n.t('app.errorgeneric') });
      }.bind(this));
    },

    downloadProject2004: function () {
      if (Origin.editor.is2004DownloadPending) {
        return;
      }
      $('.editor-common-sidebar-download2004-inner').addClass('display-none');
      $('.editor-common-sidebar-downloading2004').removeClass('display-none');
      var url = 'api/output/' + Origin.constants.outputPlugin + '/publish2004/' + this.currentCourseId;
      $.get(url, function (data, textStatus, jqXHR) {
        if (!data.success) {
          Origin.Notify.alert({
            type: 'error',
            text: Origin.l10n.t('app.errorgeneric') +
              Origin.l10n.t('app.debuginfo', { message: jqXHR.responseJSON.message })
          });
          this.resetDownloadProgress();
          return;
        }
        const pollUrl = data.payload && data.payload.pollUrl;
        if (pollUrl) {
          // Ping the remote URL to check if the job has been completed
          this.updateDownloadProgress(pollUrl);
          return;
        }
        this.resetDownload2004Progress();
        var $downloadForm = $('#downloadForm');
        $downloadForm.attr('action', 'download2004/' + Origin.sessionModel.get('tenantId') + '/' + Origin.editor.data.course.get('_id') + '/' + data.payload.zipName + '/download.zip');
        $downloadForm.submit();
      }.bind(this)).fail(function (jqXHR, textStatus, errorThrown) {
        this.resetDownload2004Progress();
        Origin.Notify.alert({ type: 'error', text: Origin.l10n.t('app.errorgeneric') });
      }.bind(this));
    },

    updatePreviewProgress: function (url, previewWindow) {
      var self = this;

      var pollUrl = function () {
        $.get(url, function (jqXHR, textStatus, errorThrown) {
          if (jqXHR.progress < "100") {
            return;
          }
          clearInterval(pollId);
          self.updateCoursePreview(previewWindow);
          self.resetPreviewProgress();
        }).fail(function (jqXHR, textStatus, errorThrown) {
          clearInterval(pollId);
          self.resetPreviewProgress();
          Origin.Notify.alert({ type: 'error', text: errorThrown });
          previewWindow.close();
        });
      }
      // Check for updated progress every 3 seconds
      var pollId = setInterval(pollUrl, 3000);
    },

    updateDownloadProgress: function (url) {
      // Check for updated progress every 3 seconds
      var pollId = setInterval(_.bind(function pollURL() {
        $.get(url, function (jqXHR, textStatus, errorThrown) {
          if (jqXHR.progress < "100") {
            return;
          }
          clearInterval(pollId);
          this.resetDownloadProgress();
        }).fail(function (jqXHR, textStatus, errorThrown) {
          clearInterval(pollId);
          this.resetDownloadProgress();
          Origin.Notify.alert({ type: 'error', text: errorThrown });
        });
      }, this), 3000);
    },

    resetPreviewProgress: function () {
      $('.editor-common-sidebar-preview-inner').removeClass('display-none');
      $('.editor-common-sidebar-previewing').addClass('display-none');
      $('.navigation-loading-indicator').addClass('display-none');
      $('.editor-common-sidebar-preview-wrapper .dropdown').removeClass('active');
      Origin.editor.isPreviewPending = false;
    },

    resetDownloadProgress: function () {
      $('.editor-common-sidebar-download-inner').removeClass('display-none');
      $('.editor-common-sidebar-downloading').addClass('display-none');
      Origin.editor.isDownloadPending = false;
    },

    resetDownload2004Progress: function () {
      $('.editor-common-sidebar-download2004-inner').removeClass('display-none');
      $('.editor-common-sidebar-downloading2004').addClass('display-none');
      Origin.editor.is2004DownloadPending = false;
    },

    resetUploadAndProgress: function () {
      $('.uploadFormSubmit').removeClass('display-none');
      $('.uploadFormSubmitandProcess').addClass('display-none');
      Origin.editor.isUploadAndProcessPenind = false;
    },

    showLoader(button, loader, type) {
      if (type == 'show') {
        $(`.${button}`).addClass('display-none');
        $(`.${loader}`).removeClass('display-none');
      } else if (type == 'hide') {
        $(`.${button}`).removeClass('display-none');
        $(`.${loader}`).addClass('display-none');
      }
    },

    updateCoursePreview: function (previewWindow) {
      var courseId = Origin.editor.data.course.get('_id');
      var tenantId = Origin.sessionModel.get('tenantId');
      previewWindow.location.href = 'preview/' + tenantId + '/' + courseId + '/';
    },

    addToClipboard: function (model) {
      var postData = {
        objectId: model.get('_id'),
        courseId: Origin.editor.data.course.get('_id'),
        referenceType: model._siblingTypes
      };
      $.post('api/content/clipboard/copy', postData, _.bind(function (jqXHR) {
        Origin.editor.clipboardId = jqXHR.clipboardId;
        this.showPasteZones(model.get('_type'));
      }, this)).fail(_.bind(function (jqXHR, textStatus, errorThrown) {
        Origin.Notify.alert({
          type: 'error',
          text: Origin.l10n.t('app.errorcopy') + (jqXHR.message ? '\n\n' + jqXHR.message : '')
        });
        this.hidePasteZones();
      }, this));
    },

    copyIdToClipboard: function (model) {
      var id = model.get('_id');

      if (helpers.copyStringToClipboard(id)) {
        Origin.Notify.alert({
          type: 'info',
          text: Origin.l10n.t('app.copyidtoclipboardsuccess', { id: id })
        });
      } else {
        Origin.Notify.alert({
          type: 'warning',
          text: Origin.l10n.t('app.app.copyidtoclipboarderror', { id: id })
        });
      }
    },

    pasteFromClipboard: function (parentId, sortOrder, layout) {
      Origin.trigger('editorView:pasteCancel');
      var postData = {
        id: Origin.editor.clipboardId,
        parentId: parentId,
        layout: layout,
        sortOrder: sortOrder,
        courseId: Origin.editor.data.course.get('_id')
      };
      $.post('api/content/clipboard/paste', postData, function (data) {
        Origin.editor.clipboardId = null;
        Origin.trigger('editorView:pasted:' + postData.parentId, {
          _id: data._id,
          sortOrder: postData.sortOrder
        });
      }).fail(function (jqXHR, textStatus, errorThrown) {
        Origin.Notify.alert({
          type: 'error',
          text: Origin.l10n.t('app.errorpaste') + (jqXHR.message ? '\n\n' + jqXHR.message : '')
        });
      });
    },

    createModel: function (type) {
      var model;
      switch (type) {
        case 'contentObjects':
          model = new ContentObjectModel();
          break;
        case 'articles':
          model = new ArticleModel();
          break;
        case 'blocks':
          model = new BlockModel();
          break;
        case 'components':
          model = new ComponentModel();
          break;
      }
      return model;
    },

    renderCurrentEditorView: function () {
      Origin.trigger('editorView:removeSubViews');

      if (this.currentView === 'menu') {
        this.renderEditorMenu();
      } else if (this.currentView === 'page') {
        this.renderEditorPage();
      }

      Origin.trigger('editorSidebarView:addOverviewView');
    },

    renderEditorMenu: function () {
      var view = new EditorMenuView({ model: Origin.editor.data.course });
      this.$('.editor-inner').html(view.$el);
    },

    renderEditorPage: function () {
      (new ContentObjectModel({
        _id: this.currentPageId
      })).fetch({
        success: function (model) {
          var view = new EditorPageView({ model: model });
          this.$('.editor-inner').html(view.$el);
        },
        error: function () {
          Origin.Notify.alert({
            type: 'error',
            text: 'app.errorfetchingdata'
          });
        }
      });
    },

    /**
    * Event handling
    */

    onEditableHoverOver: function (e) {
      e && e.stopPropagation();
      $(e.currentTarget).addClass('hovering');
    },

    onEditableHoverOut: function (e) {
      $(e.currentTarget).removeClass('hovering');
    },


  }, {
    template: 'editor'
  }
  );

  return EditorView;
});


// getAlltableData: function (params) {

//   var courseId = Origin.editor.data.course.get('_id');
//   var tenantId = Origin.sessionModel.get('tenantId');
//   var globalHeaders = new Array();
//   var courses_history = new Array();
//   var articles_history = new Array();
//   var blocks_history = new Array();
//   var components_history = new Array();
//   var contentobjects_history = new Array();

//   globalHeaders.push(["Updated on", "Type", "Action"]);


//   var table = document.createElement("TABLE");
//   table.setAttribute("class", "globalTable");
//   table.setAttribute("id", "course_table");

//   var content_table = document.createElement("TABLE");
//   content_table.setAttribute("class", "globalTable");
//   content_table.setAttribute("id", "content_table");

//   var Component_table = document.createElement("TABLE");
//   Component_table.setAttribute("class", "globalTable");
//   Component_table.setAttribute("id", "Component_table");

//   var article_table = document.createElement("TABLE");
//   article_table.setAttribute("class", "globalTable");
//   article_table.setAttribute("id", "article_table");

//   var block_table = document.createElement("TABLE");
//   block_table.setAttribute("class", "globalTable");
//   block_table.setAttribute("id", "block_table");

//   table.border = "1";
//   content_table.border = "1";
//   Component_table.border = "1";
//   block_table.border = "1";
//   article_table.border = "1";


//   var columnCount = globalHeaders[0].length;

//   //Add the header row.
//   var row = table.insertRow(-1);
//   var row_content = content_table.insertRow(-1);
//   var row_component = Component_table.insertRow(-1);
//   var row_block = block_table.insertRow(-1);
//   var row_article = article_table.insertRow(-1);

//   for (var i = 0; i < columnCount; i++) {
//     var headerCell = document.createElement("TH");
//     headerCell.innerHTML = globalHeaders[0][i];
//     row.appendChild(headerCell);
//   }

//   for (var i = 0; i < columnCount; i++) {
//     var headerCell = document.createElement("TH");
//     headerCell.innerHTML = globalHeaders[0][i];
//     row_content.appendChild(headerCell);
//   }

//   for (var i = 0; i < columnCount; i++) {
//     var headerCell = document.createElement("TH");
//     headerCell.innerHTML = globalHeaders[0][i];
//     row_component.appendChild(headerCell);
//   }

//   for (var i = 0; i < columnCount; i++) {
//     var headerCell = document.createElement("TH");
//     headerCell.innerHTML = globalHeaders[0][i];
//     row_block.appendChild(headerCell);
//   }

//   for (var i = 0; i < columnCount; i++) {
//     var headerCell = document.createElement("TH");
//     headerCell.innerHTML = globalHeaders[0][i];
//     row_article.appendChild(headerCell);
//   }


//   let getObj = { "courseId": courseId }
//   var self = this;
//   $.ajax({
//     url: 'api/gethistory',
//     type: 'POST',
//     data: getObj,
//     timeout: 420000,
//     success: function (response) {
//       console.log(response);
//       var courses_history2 = new Array();
//       var newArray = [...response.data[0].courses_history, ...response.data[0].blocks_history, ...response.data[0].components_history, ...response.data[0].contentobjects_history, ...response.data[0].courses_history]
//       console.log('Merged Array:', newArray);
//       // courses_history2 = response.data[0].courses_history;
//       self.createList(newArray);
//       if (response.status == 200) {
//         let detailed_courseData = new Array();
//         let detailed_contentData = new Array();
//         let detailed_componentData = new Array();
//         let detailed_blockData = new Array();
//         let detailed_articleData = new Array();

//         // course_data
//         response.data[0].courses_history.forEach(async element => {
//           let operation = '';
//           if (element.o == 'i') {
//             operation = 'Created';
//           }
//           if (element.o == 'u') {
//             operation = 'Updated';
//           }
//           if (element.o == 'r') {
//             operation = 'Deleted';
//           }
//           if (element.o == 'revert') {
//             operation = 'Reverted';
//           }

//           let obj = {
//             objectID: element._id,
//             operationType: operation,
//             revertDate: getnewDate(new Date(element.t))
//           }
//           courses_history.push(obj);
//           let keyObjects = Object.keys(element.updated)
//           keyObjects.forEach(innerEle => {

//             // if (innerEle != '_id' && innerEle != 'updatedAt' && innerEle != 'updatedBy') {

//             let localObj = {
//               objectID: element._id,
//               updatedAt: element.t,
//               operationType: operation,
//               ChangeType: innerEle,
//               currentData: element.updated[innerEle],
//               oldDaata: operation == 'Created' ? '' : element.old[innerEle],
//             }
//             detailed_courseData.push(localObj)
//             // }
//           });
//         });
//         //contentObject_data
//         response.data[0].contentobjects_history.forEach(async element => {
//           let operation = '';
//           if (element.o == 'i') {
//             operation = 'Created';
//           }
//           if (element.o == 'u') {
//             operation = 'Updated';
//           }
//           if (element.o == 'r') {
//             operation = 'Deleted';
//           }
//           if (element.o == 'revert') {
//             operation = 'Reverted';
//           }
//           let obj = {
//             objectID: element._id,
//             operationType: operation,
//             revertDate: getnewDate(new Date(element.t))
//           }
//           contentobjects_history.push(obj);
//           let keyObjects = Object.keys(element.updated)
//           keyObjects.forEach(innerEle => {

//             // if (innerEle != '_id' && innerEle != 'updatedAt' && innerEle != 'updatedBy') {

//             let localObj = {
//               objectID: element._id,
//               updatedAt: element.t,
//               operationType: operation,
//               ChangeType: innerEle,
//               currentData: element.updated[innerEle],
//               oldDaata: operation == 'Created' ? '' : element.old[innerEle],
//             }
//             detailed_contentData.push(localObj)
//             // }
//           });
//         });
//         // components_data
//         response.data[0].components_history.forEach(async element => {
//           let operation = '';
//           if (element.o == 'i') {
//             operation = 'Created';
//           }
//           if (element.o == 'u') {
//             operation = 'Updated';
//           }
//           if (element.o == 'r') {
//             operation = 'Deleted';
//           }
//           if (element.o == 'revert') {
//             operation = 'Reverted';
//           }
//           let obj = {
//             objectID: element._id,
//             operationType: operation,
//             revertDate: getnewDate(new Date(element.t))
//           }
//           components_history.push(obj);
//           let keyObjects = Object.keys(element.updated)
//           keyObjects.forEach(innerEle => {

//             // if (innerEle != '_id' && innerEle != 'updatedAt' && innerEle != 'updatedBy') {

//             let localObj = {
//               objectID: element._id,
//               updatedAt: element.t,
//               operationType: operation,
//               ChangeType: innerEle,
//               currentData: element.updated[innerEle],
//               oldDaata: operation == 'Created' ? '' : element.old[innerEle],
//             }
//             detailed_componentData.push(localObj)
//             // }
//           });
//         });
//         // blocks_data
//         response.data[0].articles_history.forEach(async element => {
//           let operation = '';
//           if (element.o == 'i') {
//             operation = 'Created';
//           }
//           if (element.o == 'u') {
//             operation = 'Updated';
//           }
//           if (element.o == 'r') {
//             operation = 'Deleted';
//           }
//           if (element.o == 'revert') {
//             operation = 'Reverted';
//           }
//           let obj = {
//             objectID: element._id,
//             operationType: operation,
//             revertDate: getnewDate(new Date(element.t))
//           }
//           articles_history.push(obj);
//           let keyObjects = Object.keys(element.updated)
//           keyObjects.forEach(innerEle => {

//             // if (innerEle != '_id' && innerEle != 'updatedAt' && innerEle != 'updatedBy') {

//             let localObj = {
//               objectID: element._id,
//               updatedAt: element.t,
//               operationType: operation,
//               ChangeType: innerEle,
//               currentData: element.updated[innerEle],
//               oldDaata: operation == 'Created' ? '' : element.old[innerEle],
//             }
//             detailed_articleData.push(localObj)
//             // }
//           });
//         });
//         //articles_data
//         response.data[0].blocks_history.forEach(async element => {
//           let operation = '';
//           if (element.o == 'i') {
//             operation = 'Created';
//           }
//           if (element.o == 'u') {
//             operation = 'Updated';
//           }
//           if (element.o == 'r') {
//             operation = 'Deleted';
//           }

//           if (element.o == 'revert') {
//             operation = 'Reverted';
//           }

//           let obj = {
//             objectID: element._id,
//             operationType: operation,
//             revertDate: getnewDate(new Date(element.t))
//           }
//           blocks_history.push(obj);
//           let keyObjects = Object.keys(element.updated)
//           keyObjects.forEach(innerEle => {

//             // if (innerEle != '_id' && innerEle != 'updatedAt' && innerEle != 'updatedBy') {

//             let localObj = {
//               objectID: element._id,
//               updatedAt: element.t,
//               operationType: operation,
//               ChangeType: innerEle,
//               currentData: element.updated[innerEle],
//               oldDaata: operation == 'Created' ? '' : element.old[innerEle],
//             }
//             detailed_blockData.push(localObj)
//             // }
//           });
//         });

//         for (var i = 0; i < courses_history.length; i++) {
//           var tr = document.createElement('tr');
//           var td1 = document.createElement('td');
//           var td2 = document.createElement('td');
//           var td3 = document.createElement('td');
//           var text1 = document.createTextNode(courses_history[i].revertDate);
//           var text2 = document.createTextNode(courses_history[i].operationType);

//           var element = document.createElement("input");
//           element.type = "button";
//           element.value = "Info";
//           element.name = courses_history[i].objectID;
//           element.onclick = async function (value) {
//             let obj = {
//               historyId: value.target.name,
//               modelName: 'course'
//             }
//             document.getElementById("historyIdIndex").value = value.target.name;
//             document.getElementById("content_TypeIdex").value = "course";

//             $('.modelBoxPopupDetail').addClass('showModel');
//             var newArray = detailed_courseData.filter(function (item) {
//               return item.objectID == value.target.name;
//             });

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");

//             let localheader = new Array();
//             localheader.push(["Header", "Current", "Previous"]);


//             var detailedtable = document.createElement("TABLE");
//             detailedtable.setAttribute("class", "globalTable");
//             detailedtable.setAttribute("id", "detailedTable");
//             detailedtable.border = "1";
//             var columnCountlocal = localheader[0].length;
//             var detailedRow = detailedtable.insertRow(-1);

//             for (var i = 0; i < columnCountlocal; i++) {
//               var headerCell = document.createElement("TH");
//               headerCell.innerHTML = localheader[0][i];
//               detailedRow.appendChild(headerCell);
//             }

//             for (var i = 1; i < newArray.length; i++) {
//               detailedRow = detailedtable.insertRow(-1);
//               var cell1 = detailedRow.insertCell(-1);
//               cell1.innerHTML = newArray[i].ChangeType;
//               var cell2 = detailedRow.insertCell(-1);
//               cell2.innerHTML = newArray[i].currentData;
//               var cell3 = detailedRow.insertCell(-1);
//               cell3.innerHTML = newArray[i].oldDaata;
//             }

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");
//             generatedGlobalTable.innerHTML = "";
//             generatedGlobalTable.appendChild(detailedtable);


//           };

//           td1.appendChild(text1);
//           td2.appendChild(text2);
//           td3.appendChild(element);
//           tr.appendChild(td1);
//           tr.appendChild(td2);
//           tr.appendChild(td3);
//           table.appendChild(tr);

//         }

//         // var dvTableCourses = document.getElementById("dvTableCourses");
//         // dvTableCourses.innerHTML = "";
//         // dvTableCourses.appendChild(table);


//         // contentobject table
//         for (var i = 0; i < contentobjects_history.length; i++) {
//           var tr = document.createElement('tr');

//           var td1 = document.createElement('td');
//           var td2 = document.createElement('td');
//           var td3 = document.createElement('td');
//           var text1 = document.createTextNode(contentobjects_history[i].revertDate);
//           var text2 = document.createTextNode(contentobjects_history[i].operationType);

//           // var text6 = document.creat('<input type="button" value = "Delete" onClick="Javacsript:deleteRowTable(this)">');

//           var element = document.createElement("input");
//           element.type = "button";
//           element.value = "Info";
//           element.name = contentobjects_history[i].objectID;
//           element.onclick = async function (value) {

//             document.getElementById("historyIdIndex").value = value.target.name;
//             document.getElementById("content_TypeIdex").value = "contentobject";

//             $('.modelBoxPopupDetail').addClass('showModel');
//             var newArray = await detailed_contentData.filter(function (item) {
//               return item.objectID == value.target.name;
//             });

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");

//             let localheader = new Array();
//             localheader.push(["Header", "Current", "Previous"]);


//             var detailedtable = document.createElement("TABLE");
//             detailedtable.setAttribute("class", "globalTable");
//             detailedtable.setAttribute("id", "detailedTable");
//             detailedtable.border = "1";
//             var columnCountlocal = localheader[0].length;
//             var detailedRow = detailedtable.insertRow(-1);

//             for (var i = 0; i < columnCountlocal; i++) {
//               var headerCell = document.createElement("TH");
//               headerCell.innerHTML = localheader[0][i];
//               detailedRow.appendChild(headerCell);
//             }

//             for (var i = 1; i < newArray.length; i++) {
//               detailedRow = detailedtable.insertRow(-1);
//               var cell1 = detailedRow.insertCell(-1);
//               cell1.innerHTML = newArray[i].ChangeType;
//               var cell2 = detailedRow.insertCell(-1);
//               cell2.innerHTML = newArray[i].currentData;
//               var cell3 = detailedRow.insertCell(-1);
//               cell3.innerHTML = newArray[i].oldDaata;
//             }

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");
//             generatedGlobalTable.innerHTML = "";
//             generatedGlobalTable.appendChild(detailedtable);


//           };

//           td1.appendChild(text1);
//           td2.appendChild(text2);
//           td3.appendChild(element);

//           tr.appendChild(td1);
//           tr.appendChild(td2);
//           tr.appendChild(td3);
//           content_table.appendChild(tr);
//         }

//         var dvTablePages = document.getElementById("dvTablePages");
//         dvTablePages.innerHTML = "";
//         dvTablePages.appendChild(content_table);

//         //component table
//         for (var i = 0; i < components_history.length; i++) {
//           var tr = document.createElement('tr');

//           var td1 = document.createElement('td');
//           var td2 = document.createElement('td');
//           var td3 = document.createElement('td');
//           var text1 = document.createTextNode(components_history[i].revertDate);
//           var text2 = document.createTextNode(components_history[i].operationType);

//           var element = document.createElement("input");
//           element.type = "button";
//           element.value = "Info";
//           element.name = components_history[i].objectID;
//           element.onclick = async function (value) {

//             document.getElementById("historyIdIndex").value = value.target.name;
//             document.getElementById("content_TypeIdex").value = "component";

//             $('.modelBoxPopupDetail').addClass('showModel');
//             var newArray = await detailed_componentData.filter(function (item) {
//               return item.objectID == value.target.name;
//             });

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");

//             let localheader = new Array();
//             localheader.push(["Header", "Current", "Previous"]);


//             var detailedtable = document.createElement("TABLE");
//             detailedtable.setAttribute("class", "globalTable");
//             detailedtable.setAttribute("id", "detailedTable");
//             detailedtable.border = "1";
//             var columnCountlocal = localheader[0].length;
//             var detailedRow = detailedtable.insertRow(-1);

//             for (var i = 0; i < columnCountlocal; i++) {
//               var headerCell = document.createElement("TH");
//               headerCell.innerHTML = localheader[0][i];
//               detailedRow.appendChild(headerCell);
//             }

//             for (var i = 1; i < newArray.length; i++) {
//               detailedRow = detailedtable.insertRow(-1);
//               var cell1 = detailedRow.insertCell(-1);
//               cell1.innerHTML = newArray[i].ChangeType;
//               var cell2 = detailedRow.insertCell(-1);
//               cell2.innerHTML = newArray[i].currentData;
//               var cell3 = detailedRow.insertCell(-1);
//               cell3.innerHTML = newArray[i].oldDaata;
//             }

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");
//             generatedGlobalTable.innerHTML = "";
//             generatedGlobalTable.appendChild(detailedtable);


//           };


//           td1.appendChild(text1);
//           td2.appendChild(text2);
//           td3.appendChild(element);

//           tr.appendChild(td1);
//           tr.appendChild(td2);
//           tr.appendChild(td3);

//           Component_table.appendChild(tr);
//         }

//         var dvTableComponent = document.getElementById("dvTableComponent");
//         dvTableComponent.innerHTML = "";
//         dvTableComponent.appendChild(Component_table);


//         // bloack table
//         for (var i = 0; i < blocks_history.length; i++) {
//           var tr = document.createElement('tr');

//           var td1 = document.createElement('td');
//           var td2 = document.createElement('td');
//           var td3 = document.createElement('td');
//           var text1 = document.createTextNode(blocks_history[i].revertDate);
//           var text2 = document.createTextNode(blocks_history[i].operationType);

//           var element = document.createElement("input");
//           element.type = "button";
//           element.value = "Info";
//           element.name = blocks_history[i].objectID;
//           element.onclick = async function (value) {

//             document.getElementById("historyIdIndex").value = value.target.name;
//             document.getElementById("content_TypeIdex").value = "block";

//             $('.modelBoxPopupDetail').addClass('showModel');
//             var newArray = await detailed_blockData.filter(function (item) {
//               return item.objectID == value.target.name;
//             });

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");

//             let localheader = new Array();
//             localheader.push(["Header", "Current", "Previous"]);


//             var detailedtable = document.createElement("TABLE");
//             detailedtable.setAttribute("class", "globalTable");
//             detailedtable.setAttribute("id", "detailedTable");
//             detailedtable.border = "1";
//             var columnCountlocal = localheader[0].length;
//             var detailedRow = detailedtable.insertRow(-1);

//             for (var i = 0; i < columnCountlocal; i++) {
//               var headerCell = document.createElement("TH");
//               headerCell.innerHTML = localheader[0][i];
//               detailedRow.appendChild(headerCell);
//             }

//             for (var i = 1; i < newArray.length; i++) {
//               detailedRow = detailedtable.insertRow(-1);
//               var cell1 = detailedRow.insertCell(-1);
//               cell1.innerHTML = newArray[i].ChangeType;
//               var cell2 = detailedRow.insertCell(-1);
//               cell2.innerHTML = newArray[i].currentData;
//               var cell3 = detailedRow.insertCell(-1);
//               cell3.innerHTML = newArray[i].oldDaata;
//             }

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");
//             generatedGlobalTable.innerHTML = "";
//             generatedGlobalTable.appendChild(detailedtable);


//           };

//           td1.appendChild(text1);
//           td2.appendChild(text2);
//           td3.appendChild(element);

//           tr.appendChild(td1);
//           tr.appendChild(td2);
//           tr.appendChild(td3);

//           block_table.appendChild(tr);
//         }

//         var dvTableBlock = document.getElementById("dvTableBlock");
//         dvTableBlock.innerHTML = "";
//         dvTableBlock.appendChild(block_table);


//         // article table
//         for (var i = 0; i < articles_history.length; i++) {
//           var tr = document.createElement('tr');

//           var td1 = document.createElement('td');
//           var td2 = document.createElement('td');
//           var td3 = document.createElement('td');
//           var text1 = document.createTextNode(articles_history[i].revertDate);
//           var text2 = document.createTextNode(articles_history[i].operationType);

//           var element = document.createElement("input");
//           element.type = "button";
//           element.value = "Info";
//           element.name = articles_history[i].objectID;
//           element.onclick = async function (value) {

//             document.getElementById("historyIdIndex").value = value.target.name;
//             document.getElementById("content_TypeIdex").value = "article";

//             $('.modelBoxPopupDetail').addClass('showModel');
//             var newArray = await detailed_articleData.filter(function (item) {
//               return item.objectID == value.target.name;
//             });

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");

//             let localheader = new Array();
//             localheader.push(["Header", "Current", "Previous"]);


//             var detailedtable = document.createElement("TABLE");
//             detailedtable.setAttribute("class", "globalTable");
//             detailedtable.setAttribute("id", "detailedTable");
//             detailedtable.border = "1";
//             var columnCountlocal = localheader[0].length;
//             var detailedRow = detailedtable.insertRow(-1);

//             for (var i = 0; i < columnCountlocal; i++) {
//               var headerCell = document.createElement("TH");
//               headerCell.innerHTML = localheader[0][i];
//               detailedRow.appendChild(headerCell);
//             }

//             for (var i = 1; i < newArray.length; i++) {
//               detailedRow = detailedtable.insertRow(-1);
//               var cell1 = detailedRow.insertCell(-1);
//               cell1.innerHTML = newArray[i].ChangeType;
//               var cell2 = detailedRow.insertCell(-1);
//               cell2.innerHTML = newArray[i].currentData;
//               var cell3 = detailedRow.insertCell(-1);
//               cell3.innerHTML = newArray[i].oldDaata;
//             }

//             var generatedGlobalTable = document.getElementById("generatedGlobalTable");
//             generatedGlobalTable.innerHTML = "";
//             generatedGlobalTable.appendChild(detailedtable);
//           };

//           td1.appendChild(text1);
//           td2.appendChild(text2);
//           td3.appendChild(element);

//           tr.appendChild(td1);
//           tr.appendChild(td2);
//           tr.appendChild(td3);
//           article_table.appendChild(tr);
//         }

//         var dvTableCourses = document.getElementById("dvTableArticle");
//         dvTableArticle.innerHTML = "";
//         dvTableArticle.appendChild(article_table);
//       }


//     },
//     error: function (XMLHttpRequest, textStatus, errorThrown) {
//       $loading.addClass('display-out');
//     }
//   });

// },
